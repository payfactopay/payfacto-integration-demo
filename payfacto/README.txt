INTRODUCTION
============

These files demonstrate two different methods for displaying Secure Fields: as
a modal, and as embedded fields. Both methods use the same client code to
communicate with the server. There are two server files: app.js and app.php.
The app.js server file is a nodejs application. The app.php file is a PHP
application. Both server files perform identical operations; obtain a Secure
Fields token, perform a purchase with token, and perform acknowledgement.
In addition, the app.js file acts as a primitive HTTP server, serving up static
content. The app.php file relies on Apache to serve up static content.

MANIFEST
========
.htaccess                 - mod_rewrite file, used to map the client calls to
                            the app.php application
app.js                    - nodejs server file, used to communicate with
                            PayFacto's servers
app.php                   - PHP server file, used to communicate with
                            PayFacto's servers
atl.site.logo             - PayFacto logo
client.css                - CSS common to demonstration pages
displayEmbedded.html      - page demonstrating how to embed Secure Fields in
                            the page
displayEmbeddedModal.html - page containing the embedded Secure Fields
displayModal.html         - page demonstrating how to display the Secure Fields
                            modal
index.html                - page containing links to different examples
package.json              - manifest of packages required to run nodejs
                            version of server
README.txt                - this file

INSTALLATION
============

While app.js and app.php use the same front-end files on the client side, they
have very requirements for the back-end.

nodejs Installation
--------------------

The nodejs server file requires nodejs to be installed.

To run app.js on your localhost do the following:
- unzip the archive, a directory named 'payfacto' will be created
- edit the file 'payfacto/app.js' and add your company number, merchant number,
  and API Key to the constants at the top of the file
- change directories into the 'payfacto' directory
- run the command `npm install`
- to start the server run the command `node app.js`

Using a browser navigate to 'http://localhost:8090/index.html'. The
introduction page will be available for testing.

PHP Installation
----------------

The PHP file requires Apache to be installed running on port 80 and PHP, and
mod_rewrite to be available.

To run app.php on your localhost do the following:
- unzip the archive into the Apache document root, a directory named 'payfacto'
  will be created
- edit the file 'payfacto/app.php' and add your company number, merchant
  number, and API Key to the constants at the top of the file

Using a browser navigate to 'http://localhost/index.html'. The
introduction page will be available for testing.

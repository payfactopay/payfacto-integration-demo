// propertes used in requesting a new Secure Fields session (i.e. /pafacto/session)
function getInitializationProperties() {
  return {
    method: 'GET',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Accept': 'application/json',
    }
  }
}

// In Payfacto.init call. Values are hard coded to staging servers
function getInitOptions() {
  return {
    apiUrl: 'https://test.api.payfacto.com/v1',
    iframeUrl: 'https://test.form.payfacto.com/SecureFields'
  }
}

function createPayFactoModal(submitSuccess, submitFailure) {
  const modalOptions = {
    // If you wish to customize the modal through options they go here
  };
  return window.PayFacto.modal(token => doPurchase(token, submitSuccess, submitFailure), modalOptions);
}

function doPurchase(token, submitSuccess, submitFailure) {
  // Some Apache setups return the response as a JSON string instead of a JSON object.
  // If a JSON string is returned it is caught and parsed into an object via JSON.parse().
  token.then(data => {
    fetch('/payfacto/purchase', getPurchaseProperties(data))
      .then(response => response.json())
      .then(response => typeof(response) === "string" ? JSON.parse(response) : response)
      .then(response => sendAcknowledge(response, submitSuccess, submitFailure))
      .catch(error => submitFailure(error));
  }).catch(error => submitFailure(error));
}

function getPurchaseProperties(data) {
  return getPostProperties(JSON.stringify({
  'token': data.token,
  'purchaseAmount': '00000001000',
  'purchaseCurrency': 'CAD'
  }));
}

function sendAcknowledge(purchaseResults, submitSuccess, submitFailure) {
  const {companyNumber, merchantNumber, transactionNumber} = {...purchaseResults};
  fetch('/payfacto/acknowledge', getAcknowledgeProperties(companyNumber, merchantNumber, transactionNumber))
    .then(response => response.json())
    .then(response => typeof(response) === "string" ? JSON.parse(response) : response)
    .then(response => submitSuccess(response))
    .catch(error => submitFailure(error));
}

function getAcknowledgeProperties(companyNumber, merchantNumber, transactionNumber) {
  return getPostProperties(JSON.stringify({
  'CompanyNumber': companyNumber,
  'MerchantNumber': merchantNumber,
  'TransactionNumber': transactionNumber
  }));
}

function getPostProperties(body) {
  return {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'cache': 'no-cache'
    },
    body: body
  };
}

function showSuccess(modal, response) {
  modal.showSuccess(response.errorDescription);
}

function showError(modal, error) {
  modal.showError(error.returnCode + ': ' + error.errorDescription);
}

const express = require('express');
const fetch = require('node-fetch');
const bodyParser = require('body-parser');

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

const apiUrl = 'https://test.api.payfacto.com/v1';
const authApiKey = 'YOUR_API_KEY_HERE';
const companyNumber = 'COMPANY_NUMBER';
const merchantNumber = 'MERCHANT_NUMBER';

const app = express();
app.disable('x-powered-by');
app.use(express.json());
app.use(bodyParser.urlencoded({ extended: true }));

// retrieve the HTML pages
app.get('/', function (req, res) {
  res.sendFile(__dirname + '/index.html');
});

app.get('/payfacto/atl.site.logo', function (req, res) {
  res.sendFile(__dirname + '/atl.site.logo');
});

app.get('/payfacto/index.html', function (req, res) {
  res.sendFile(__dirname + '/index.html');
});

app.get('/payfacto/client.js', function (req, res) {
  res.sendFile(__dirname + '/client.js');
});

app.get('/payfacto/client.css', function (req, res) {
  res.sendFile(__dirname + '/client.css');
});

app.get('/payfacto/displayModal.html', function (req, res) {
  res.sendFile(__dirname + '/displayModal.html');
});

app.get('/payfacto/displayEmbedded.html', function (req, res) {
  res.sendFile(__dirname + '/displayEmbedded.html');
});

app.get('/payfacto/displayEmbeddedModal.html', function (req, res) {
  res.sendFile(__dirname + '/displayEmbeddedModal.html');
});

// everything below this point is to actually process a transaction
// the rest of the code is in index.html
app.get('/payfacto/session', (req, res) => {
  const sessionUrl = apiUrl + '/securefields/session/new';
  const sessionProperties = getRequestProperties(getDefaultPayload());

  fetch(sessionUrl, sessionProperties)
    .then(response => response.json())
    .then(response => res.send({ secureID: response.secureID }))
    .catch(response => console.error(response));
});

app.post('/payfacto/purchase', bodyParser.json(), (req, res) => {
  const purchaseUrl = apiUrl + '/purchaseWithToken';
  const purchaseProperties = getPurchasePayload();
  purchaseProperties.Token = req.body.token;
  const purchasePayload = getRequestProperties(purchaseProperties);

  fetch(purchaseUrl, purchasePayload)
      .then(response => response.json())
      .then(response => res.send(response))
      .catch(response => console.error(response));
});

app.post('/payfacto/acknowledge', bodyParser.json(), (req, res) => {
  const acknowledgeUrl = apiUrl + '/ack';
  const acknowledgeProperties = getRequestProperties({
    CompanyNumber: getDefaultPayload().CompanyNumber,
    MerchantNumber: getDefaultPayload().MerchantNumber,
    TransactionNumber: req.body.TransactionNumber
  });

  fetch(acknowledgeUrl, acknowledgeProperties)
    .then(response => response.json())
    .then(response => res.send(response))
    .catch(response => console.error(response));
});



function getRequestProperties(payload) {
  return {
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Accept': 'application/json',
      'Access-Control-Allow-Origin': '*'
    },
    body: 'auth-api-key=' + authApiKey + '&payload=' + asBase64(payload)
  }
}

function asBase64(payload) { return Buffer.from(toKeyValuePair(payload).join('&')).toString('base64') }

function toKeyValuePair(payload) { return Object.keys(payload).map(key => key + '=' + payload[key]) }

function getPurchasePayload() {
  let payload = getDefaultPayload();
  payload.Amount = '00000001000';
  payload.CurrencyCode = 'CAD';
  payload.InvoiceNumber = Math.floor(new Date().getTime() / 10);
  payload.InputType = 'I';
  payload.MerchantTerminalNumber = '     ';
  payload.LanguageCode = 'F';

  return payload;
}

function getDefaultPayload() {
  return {
    CompanyNumber: companyNumber,     // required
    MerchantNumber: merchantNumber,   // required
    CustomerNumber: '00000001',       // required
    OperatorID: 'PAYFACTO'            // required
  };
}

app.listen(8090);

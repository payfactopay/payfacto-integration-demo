<?php
error_reporting(~0);
ini_set('display_errors', 1);

define('apiUrl', 'https://test.api.payfacto.com/v1');
define('authApiKey', 'Put your API key here');
define('companyNumber', 'Put 5 digit company number here');
define('merchantNumber', 'Put 8 digit merchant number here');

// Simple tests to determine if we are creating a token, purchasing an item, or acknowledgineg the receipt of purchase
if ($_SERVER["REQUEST_URI"] == "/payfacto/session") {
  getSecureFieldsSession();
} else if ($_SERVER["REQUEST_URI"] == "/payfacto/purchase") {
  doPurchase();
} else if ($_SERVER["REQUEST_URI"] == "/payfacto/acknowledge") {
  doAck();
} else {
  echo "Unkown request URI: " . $_SERVER["REQUEST_URI"];
}

function getSecureFieldsSession() {
  $url = apiUrl . '/securefields/session/new';
  $content = [
    'CompanyNumber' => companyNumber,
    'MerchantNumber' => merchantNumber,
    'CustomerNumber' => '00000001',
    'OperatorID' => 'OPERATOR'
  ];
  $context = stream_context_create(buildOptions($content));
  $response = file_get_contents($url, false, $context);

  // only the secureID needs to be returned
  header('Content-Type: application/json; charset=utf-8');
  echo json_encode([ 'secureID' => json_decode($response)->secureID]);
}

function doPurchase() {
  $request = getPostArguments();
  $url = apiUrl . '/purchaseWithToken';
  $content = [
    'CompanyNumber' => companyNumber,
    'MerchantNumber' => merchantNumber,
    'CustomerNumber' => '00000001',
    'OperatorID' => 'OPERATOR',
    'Amount' => '00000001000',
    'CurrencyCode' => 'CAD',
    'InvoiceNumber' => substr(preg_replace("/[^0-9]/", "", microtime()), -12),
    'InputType' => 'I',
    'MerchantTerminalNumber' => '     ',
    'LanguageCode' => 'F',
    'Token' => $request->token
  ];
  callPayfactoServerAndReturnResults($url, $content);
}

function doAck() {
  $request = getPostArguments();
  $url = apiUrl . '/ack';
  $content = [
    'CompanyNumber' => companyNumber,
    'MerchantNumber' => merchantNumber,
    'TransactionNumber' => $request->TransactionNumber
  ];
  callPayfactoServerAndReturnResults($url, $content);
}

function getPostArguments() {
  return json_decode(file_get_contents('php://input'));
}

function callPayfactoServerAndReturnResults($url, $content) {
  $context = stream_context_create(buildOptions($content));
  $response = file_get_contents($url, false, $context);
  header('Content-Type: application/json; charset=utf-8');
  echo json_encode($response);
}

function buildOptions($parameters) {
  // note we add a User-Agent as get_file_contents does not add one
  return [
    'http' => [
      'method' => 'POST',
      'ignore_errors' => true,
      'header' => [
        'Content-Type: application/x-www-form-urlencoded',
        'Accept: application/json',
        'Access-Control-Allow-Origin: *',
        'User-Agent: curl/7.64.1'
      ],
      'content' => buildContent($parameters)
    ]
  ];
}

function buildContent($parameters) {
  // http_build_query is a fast way of converting an array to keyvalue pairs but spaces must remain spaces
  return 'auth-api-key=' . authApiKey . '&payload=' . 
      base64_encode(str_replace('%20', ' ', http_build_query($parameters, '', null, PHP_QUERY_RFC3986)));
}
?>
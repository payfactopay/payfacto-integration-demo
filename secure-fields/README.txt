INTRODUCTION
============

These files demonstrate different ways to customize SecureFields to your needs.
All examples are fully working and use the same nodejs backend to call 
PayFacto's APIs.

MANIFEST
========

3DS2.html                 - example of the parameters needed for a 3DS2
                            transaction
README.txt                - this file
SPA.html                  - example of how to use SecureFields with a single
                            page application
address.html              - example of how to display the address and postal
                            code fields
app.js                    - nodejs server file, used to communicate with
                            PayFacto's servers
common.css                - CSS layouts common to all example files
common.js                 - JavaScript code common to all example files
custom-placeholders.html  - example of how to replace the default placeholder
                            with custom placeholders
customize-1.html          - example showing how to modify the look using CSS
customize-2.html          - example showing how to modify the look using CSS
customize-3.html          - example showing how to modify the look using CSS
email.html                - example of how to display the email field
embedded-iframe.html      - form containing iframe in embedded iframe example
embedded.html             - example showing how to embed model within a page
fixed-labels.html         - example showing how to prevent fields from floating
index.html                - main page containing links to examples
messages.html             - example showing how to customize the result messages
package-lock.json         - nodejs file
package.json              - nodejs file
pan19.html                - example showing how to accept a 19 digit credit 
                            card number

INSTALLATION
============

The nodejs server file requires nodejs to be installed.

To run app.js on your localhost do the following:
- edit the file app.js and add your company number, merchant number,
  and API Key to the constants at the top of the file
- run the command `npm install`
- to start the server run the command `node app.js`

Using a browser navigate to 'http://localhost:8090/index.html'. The
main page will be displayed. Use the links on the page to navigate through 
the examples.

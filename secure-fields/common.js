/*
  initializeSession calls the server to get a new secure fields session. The session is used to link the credit card 
  number to the merchant. This is required to generate the token after the credit card information is submitted via
  the pay button.
*/
function initializeSession() {
  console.log('initializeSession');
  fetch('/payfacto/session', getInitializationProperties())
    .then(response => response.json())
    .then(response => JSON.parse(JSON.stringify(response)))
    .then(response => initPayfacto(response))
    .catch(response => console.error(response));
}

function initPayfacto(response) {
  console.log(response);
  window.PayFacto.init(response.secureID, getInitOptions())
    .then(success => console.log(success))
    .catch(error => console.error(error));
}

function getInitOptions() {
  // The 'permanent' property is used to indicate whether or not the token should be a termporary token or a permanent
  // token. The default is false (temporary.)
  // The 'lang' property can be set to either "en" or "fr".
  return {
    lang: 'en',
    permanent: true,
    apiUrl: 'https://test.api.payfacto.com/v1',
    iframeUrl: 'https://test.form.payfacto.com/SecureFields'
  }
}

/*
  Used in the SPA example to create a new secure fields session without having to reload the page.
*/
function reinitializeSession() {
  console.log('initializeSession');
  fetch('/payfacto/session', getInitializationProperties())
    .then(response => response.json())
    .then(response => JSON.parse(JSON.stringify(response)))
    .then(response => resetPayfacto(response))
    .catch(response => console.error(response));
}

function getInitializationProperties() {
  return {
    method: 'GET',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Accept': 'application/json',
    }
  }
}

// this is the call that actually resets the session
function resetPayfacto(response) {
  window.PayFacto.resetSecureId(response.secureID)
}

/*
  When the token is returned by Secure Fields, the purchase can be made. The token can also be used with the 
  /preauthWithToken call.
*/
function doPurchase(token, submitSuccess, submitFailure) {
  let accumulatedResults = {};
  token.then(data => {
    accumulatedResults.modalResults = data;
    fetch('/payfacto/purchase', getPurchaseProperties(data))
      .then(response => response.json())
      .then(response => {
        accumulatedResults.purchaseResults = response;
        sendAcknowledge(accumulatedResults, submitSuccess, submitFailure);
      })
      .catch(error => submitFailure(error));
  }).catch(error => submitFailure(error));
}

function getPurchaseProperties(data) {
  return getPostProperties(JSON.stringify({
  'token': data.token,
  'purchaseAmount': '00000001000',
  'purchaseCurrency': 'CAD'
  }));
}

/*
  When the purchase request is successful then an acknowledge must be sent. Otherwise the system will assume the 
  purchase response was never received and reverse the transaction.
*/
function sendAcknowledge(purchaseResults, submitSuccess, submitFailure) {
  const {companyNumber, merchantNumber, transactionNumber} = {...purchaseResults};
  fetch('/payfacto/acknowledge', getAcknowledgeProperties(companyNumber, merchantNumber, transactionNumber))
    .then(response => response.json())
    .then(response => submitSuccess(response))
    .catch(error => submitFailure(error));
}

function getAcknowledgeProperties(companyNumber, merchantNumber, transactionNumber) {
  return getPostProperties(JSON.stringify({
  'CompanyNumber': companyNumber,
  'MerchantNumber': merchantNumber,
  'TransactionNumber': transactionNumber
  }));
}

function getPostProperties(body) {
  return {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'cache': 'no-cache'
    },
    body: body
  };
}

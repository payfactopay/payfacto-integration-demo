# Using Secure Fields
Secure Fields requires the following setup:

1. Include the Secure Fields JavaScript and CSS files in your HTML document’s HEAD tag.

```
<link rel="stylesheet" href="https://test.form.payfacto.cloud/securefields/1.5.2/payfacto-secure-fields.css"/>
<script src="https://test.form.payfacto.cloud/securefields/1.5.2/payfacto-secure-fields.js"></script>
```
2. When ready to display the modal, call the Secure Fields new session endpoint to obtain a temporary Secure Fields token.

3. Call PayFacto.init using the temporary Secure Fields token.

```
PayFacto.init(token, {
  lang: 'en',
  apiUrl: 'https://test.api.payfacto.cloud/v1',
  iframeUrl: 'https://test.form.payfacto.cloud/SecureFields'
});
```
4.Call PayFacto.modal to display the modal.

```
PayFacto.modal(response => {...});
```
The response object for the callback function contains masked values of the inputs received by the server from the modal plus a token that can be used for a call to pre-authorization with token or purchase with token endpoints.
## Secure Fields Flow
![Secure Fields Flow](./img/secureFieldsFlow.png)

## Customizing Secure Fields
Secure Fields can be customized in two different ways; presentation and logic. Both can be done by passing in different options. Those options are listed below.
### Customizing the Presentation
There are two ways to customize the presentation, through CSS and through modal options.
#### Customizing Through CSS
To add custom CSS to the modal simply define the CSS with an id (i.e. `#payfacto-secure-fields {...}` ) and then provide that id to the modal as an option.
```
PayFacto.modal(token => callback(token), { id: 'payfacto-secure-fields' })
```
The modal will be prefixed with the id allowing the CSS to be applied to it.

There are a number of different classes that can be customized. To customize a particular element it is recommended that you inspect the element and see what classes are applied. Below is a table containing some of the more common classes:

| Class | Description |
|---|---|
| payfacto-input-simple | Applies to the input fields in the modal |
| payfacto-object | Applies to all objects in the modal |
| pf-btn | Applies to all buttons display in the modal |
| pf-modal-btn-confirm | Used on the “Pay” button at the bottom of the modal |
| pf-modal-content | Applies to the main area of the modal |
| pf-modal-footer | Footer area at the bottom of the modal |
| pf-modal-header | Header area at the top of the modal |
| pf-modal-header-logo | img tag containing logo on the top of the modal |

There are also classes that apply to specific fields in the modal:

| Class | Description |
|---|---|
| payfacto-card | Class for the credit card field |
| payfacto-cvv | Class for the verification code field |
| payfacto-expiration-date | Class for the expiry date |
| payfacto-name | Class for the Name field |

#### Customizing Through Options
Secure Fields has a number of options in order to customize the look of the modal. The options are passed in as a parameter object on the PayFacto.modal call or the PayFacto.init call.

```
PayFacto.init(secureId, initOptions);
PayFacto.modal(token => callback(token), modalOptions);
```
Below are the init options:

| Property | Values | Description |
|---|---|---|
| lang | ‘en’, ‘fr’ | Language to display field names in. Defaults to English |
| permanent | boolean | Should the generate payment token be temporary or permanent. Defaults to temporary (false.) |
| apiUrl | URL | URL to the appropriate Gateway API (i. e. https://test.api.payfacto.com/v1.) This value is mandatory. |
| iframeUrl | URL | URL to the appropriate Secure Fields server (i. e. https://test.form.payfacto.com/SecureFields.) This value is mandatory. |

Below are the modal options:

| Property | Values | Description |
|---|---|---|
| id | String | Element id to affix to Secure Fields modal. |
| classes | String | Space separated list of class name to affix to Secure Fields modal. |
| purchaseAmount | Integer | Amount of purchase. Must be greater than or equal to 0. Required for 3DS2 transactions. Ignored otherwise. |
| purchaseCurrency | ‘CAD’, ‘USD’ | Currency of purchase. Required for 3DS2 transactions. Ignored otherwise. |
| purchaseDate | String | Date of purchase in UTC format (i. e. YYYY-MM-DDTHH:MM:SS-HH:00.) Required for 3DS2 transactions. Ignored otherwise. |
| autoClose | boolean | When set to true modal is automatically closed after a response is received from submitting the credentials. |
| onsubmitValidation | boolean \| function | When set to false fields are not validated before they are submitted. When set to a function the function is passed a boolean indicating if there is an error or not. |
| header | Object | Contains properties that affect the header section of the modal. |
| header.logo | String \| Element | Logo to appear at the top of the modal. If header.logo is a string the string is embedded as the src attribute in an img tag. If header.logo is an Element the Element is embedded in the header logo position. |
| header.title | String | Title to appear in the modal header below the logo. |
| header.subtitle | String | Subtitle to appear in the modal header below the title. |
| sections | Object | Contains properties that affect the body of the modal. |
| sections.address | boolean | When set to true the Address and Postal Code fields will be displayed. |
| sections.address.address | boolean | When set to true the Address field is displayed. |
| sections.address.postal-code | boolean | When set to true the Postal Code field is displayed. |
| sections.other | boolean | When set to true the Email Address field will be displayed. |
| sections.credit | boolean | When set to false the Cardholder Name, Card Number, Card Expiration, and Verification Code fields are hidden. |
| sections.credit.name | boolean | When set to false the Cardholder Name field is hidden. |
| sections.credit.card-number | boolean | When set to false the Card Number field is hidden. |
| sections.credit.expiration-date | boolean | When set to false the Card Expiration field is hidden. |
| sections.credit.cvv | boolean | When set to false the Verification Code field is hidden. |
| fields | Object | Contains properties that will affect fields in the modal. |
| fields.name | Object | Contains properties that will affect the Cardholder Name field. |
| fields.name.icon | String | Override default font awesome icon with a different one. |
| fields.name.name | String | Override the name of the field. |
| fields.name.title.floating | boolean | When set to false the title stays above the input box instead of floating up from inside the box. |
| fields.name.title.text | String | Override the title text. |
| fields.name.feedback | boolean | When set to false the feedback icon on the right side of the input box is hidden. |
| fields.name.autocapitalize | none \| sentences \| words \| characters | Overrides the default autocapitalize setting of the input box. Note that this feature is not supported by all browsers. |
| fields.name.autocomplete | String | Override the default HTML autocomplete setting on the input box. For possible values please see the HTML reference guide. |
| fields.name.style | Object | Override the default style with an Object that contains CSS key/value pairs. |
| fields.name.type | String | Override the default input type. Must be a valid input type. |
| fields.name.mask.placeholder | String | Adds placeholder text to the input box. |
| fields.card-number | Object | Contains properties that will affect the Card Number field |
| fields.card-number.icon | String | Override default font awesome icon with a different one. |
| fields.card-number.name | String | Override the name of the field. |
| fields.card-number.title.floating | boolean | When set to false the title stays above the input box instead of floating up from inside the box. |
| fields.card-number.title.text | String | Override the title text. |
| fields.card-number.feedback | boolean | When set to false the feedback icon on the right side of the input box is hidden. |
| fields.card-number.autocapitalize | none \| sentences \| words \| characters | Overrides the default autocapitalize setting of the input box. Note that this feature is not supported by all browsers. |
| fields.card-number.autocomplete | String | Override the default HTML autocomplete setting on the input box. For possible values please see the HTML reference guide. |
| fields.card-number.style | Object | Override the default style with an Object that contains CSS key/value pairs. |
| fields.card-number.type | String | Override the default input type. Must be a valid input type. |
| fields.card-number.mask.none | boolean | When set to false mask is removed. |
| fields.card-number.mask.placeholder | String | Adds placeholder text to the input box. |
| fields.expiration-date | Object | Contains properties that will affect the Card Expiration field. |
| fields.expiration-date.icon | String | Override default font awesome icon with a different one. |
| fields.expiration-date.name | String | Override the name of the field. |
| fields.expiration-date.title.floating | boolean | When set to false the title stays above the input box instead of floating up from inside the box. |
| fields.expiration-date.title.text | String | Override the title text. |
| fields.expiration-date.feedback | boolean | When set to false the feedback icon on the right side of the input box is hidden. |
| fields.expiration-date.autocapitalize | none \| sentences \| words \| characters | Overrides the default autocapitalize setting of the input box. Note that this feature is not supported by all browsers. |
| fields.expiration-date.autocomplete | String | Override the default HTML autocomplete setting on the input box. For possible values please see the HTML reference guide. |
| fields.expiration-date.style | Object | Override the default style with an Object that contains CSS key/value pairs. |
| fields.expiration-date.type | String | Override the default input type. Must be a valid input type. |
| fields.expiration-date.mask.placeholder | String | Adds placeholder text to the input box. |
| fields.cvv | Object | Contains properties that will affect the Verification Code field. |
| fields.cvv.icon | String | Override default font awesome icon with a different one. |
| fields.cvv.name | String | Override the name of the field. |
| fields.cvv.title.floating | boolean | When set to false the title stays above the input box instead of floating up from inside the box. |
| fields.cvv.title.text | String | Override the title text. |
| fields.cvv.feedback | boolean | When set to false the feedback icon on the right side of the input box is hidden. |
| fields.cvv.autocapitalize | none \| sentences \| words \| characters | Overrides the default autocapitalize setting of the input box. Note that this feature is not supported by all browsers. |
| fields.cvv.autocomplete | String | Override the default HTML autocomplete setting on the input box. For possible values please see the HTML reference guide. |
| fields.cvv.style | Object | Override the default style with an Object that contains CSS key/value pairs. |
| fields.cvv.type | String | Override the default input type. Must be a valid input type. |
| fields.cvv.mask.placeholder | String | Adds placeholder text to the input box. |
| fields.address | Object | Contains properties that will affect the Cardholder Address field. |
| fields.address.icon | String | Override default font awesome icon with a different one. |
| fields.address.name | String | Override the name of the field. |
| fields.address.title.floating | boolean | When set to false the title stays above the input box instead of floating up from inside the box. |
| fields.address.title.text | String | Override the title text. |
| fields.address.feedback | boolean | When set to false the feedback icon on the right side of the input box is hidden. |
| fields.address.autocapitalize | none \| sentences \| words \| characters | Overrides the default autocapitalize setting of the input box. Note that this feature is not supported by all browsers. |
| fields.address.autocomplete | String | Override the default HTML autocomplete setting on the input box. For possible values please see the HTML reference guide. |
| fields.address.style | Object | Override the default style with an Object that contains CSS key/value pairs. |
| fields.address.type | String | Override the default input type. Must be a valid input type. |
| fields.address.mask.placeholder | String | Adds placeholder text to the input box. |
| fields.postal-code | Object | Contains properties that will affect the Cardholder Postal Code field. |
| fields.postal-code.icon | String | Override default font awesome icon with a different one. |
| fields.postal-code.name | String | Override the name of the field. |
| fields.postal-code.title.floating | boolean | When set to false the title stays above the input box instead of floating up from inside the box. |
| fields.postal-code.title.text | String | Override the title text. |
| fields.postal-code.feedback | boolean | When set to false the feedback icon on the right side of the input box is hidden. |
| fields.postal-code.autocapitalize | none \| sentences \| words \| characters | Overrides the default autocapitalize setting of the input box. Note that this feature is not supported by all browsers. |
| fields.postal-code.autocomplete | String | Override the default HTML autocomplete setting on the input box. For possible values please see the HTML reference guide. |
| fields.postal-code.style | Object | Override the default style with an Object that contains CSS key/value pairs. |
| fields.postal-code.type | String | Override the default input type. Must be a valid input type. |
| fields.postal-code.mask.placeholder | String | Adds placeholder text to the input box. |
| fields.email | Object | Contains properties that will affect the Email field. |
| fields.email.icon | String | Override default font awesome icon with a different one. |
| fields.email.name | String | Override the name of the field. |
| fields.email.title.floating | boolean | When set to false the title stays above the input box instead of floating up from inside the box. |
| fields.email.title.text | String | Override the title text. |
| fields.email.feedback | boolean | When set to false the feedback icon on the right side of the input box is hidden. |
| fields.email.autocapitalize | none \| sentences \| words \| characters | Overrides the default autocapitalize setting of the input box. Note that this feature is not supported by all browsers. |
| fields.email.autocomplete | String | Override the default HTML autocomplete setting on the input box. For possible values please see the HTML reference guide. |
| fields.email.style | Object | Override the default style with an Object that contains CSS key/value pairs. |
| fields.email.type | String | Override the default input type. Must be a valid input type. |
| fields.email.mask.placeholder | String | Adds placeholder text to the input box. |
| fields.all | Object | Contains properties that will affect all displayed fields. |
| fields.all.icon | String | Override default font awesome icon with a different one for all fields. |
| fields.all.name | String | Override the names of the fields. |
| fields.all.title.floating | boolean | When set to false the title stays above the input box instead of floating up from inside the box. |
| fields.all.title.text | String | Override the title text. |
| fields.all.feedback | boolean | When set to false the feedback icon on the right side of the input box is hidden. |
| fields.all.autocapitalize | none \| sentences \| words \| characters | Overrides the default autocapitalize setting of the input box. Note that this feature is not supported by all browsers. |
| fields.all.autocomplete | String | Override the default HTML autocomplete setting on the input box. For possible values please see the HTML reference guide. |
| fields.all.style | Object | Override the default style with an Object that contains CSS key/value pairs. |
| fields.all.type | String | Override the default input type. Must be a valid input type. |
| fields.all.mask.placeholder | String | Adds placeholder text to the input box. |
